$(document).ready(function(){
    initSlugGenerator();
});

function initSlugGenerator(){
    var for_slug = $('.for_slug');
    var slug = $('.slug');
    var slugGeneratorEnabled = false;

    if(slug.val() == ''){
        slugGeneratorEnabled = true;
    }

    if(slugGeneratorEnabled){
        slug.val(transliterate(for_slug.val()));
    }

    for_slug.on('input',function(){
        if(slugGeneratorEnabled){
            slug.val(transliterate(for_slug.val()));
        }
    });
}

