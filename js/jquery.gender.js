jQuery.fn.gender = function (options) {
    // настройки по умолчанию
    var options = jQuery.extend({
        maleFieldId: 'male', // идентификатор поля мужской пол
        femaleFieldId: 'female' // идентификатор поля женский пол
    }, options);

    function getGenderByMiddleName(middleName) {
        var ending = middleName.substr(middleName.length - 2).toLowerCase();
        if (ending == 'на' || ending == 'зы')
            return "Female";
        else if (ending == 'ич' || ending == 'лы')
            return "Male";
        return null;
    }

    return this.each(function() {
        $(this).blur(function() {
            var result = getGenderByMiddleName($(this).val());
            if (result == "Male") {
                $('#' + options.maleFieldId).attr('checked', 'checked');
                $('#' + options.femaleFieldId).removeAttr('checked');
            }
            else if (result == "Female") {
                $('#' + options.femaleFieldId).attr('checked', 'checked');
                $('#' + options.maleFieldId).removeAttr('checked');
            }

        });

    });
};