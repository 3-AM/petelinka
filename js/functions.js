$(document).ready(function(){
    /* GA */
    $('#registration_form').submit(function(){
        gaSendRegisterForm();
    });

    $('.rules_link').click(function(){
        gaRulesLink();
        return true;
    });

    $('.order_link').click(function(){
        gaOrderLink();
        return true;
    });

    $('.buy_event').click(function(){
        gaBuyLink();
        return true;
    });
    /* end GA */

    arrowCheck(0);
    $('.prev, .next, .navigation li a').click(function(){
        arrowCheck(500);
    });

    //video stop
    $('.prev, .next').click(function(){
        /*$('object').each(function(){
            $(this).html($(this).html());
        });*/
        ytplayers = $('.ytplayer');
        ytplayers.each(function(){
            ytplayer = document.getElementById($(this).attr('id'));
            ytplayer.pauseVideo();
        });
    });

    $('#more_codes').click(function(){
        $('#table_codes tr').show();
        $(this).parent().remove();
        return false;
    });

    $('#captcha_refresh').click(function(){
        var number = Math.floor(Math.random()*10001);
        $('#captcha_image').attr('src', $(this).attr('data-url') + '?param=' + number);
    });
});

function gaSendRegisterForm(){
    _gaq.push(['_trackEvent', 'form', 'fill']);
}

function gaSuccessRegisterCode(){
    _gaq.push(['_trackEvent', 'form', 'code success']);
}

function gaVideoClick(){
    _gaq.push(['_trackEvent', 'video', 'play']);
}

function gaRulesLink(){
    _gaq.push(['_trackEvent', 'button', 'rules']);
}

function gaOrderLink(){
    _gaq.push(['_trackEvent', 'order', 'money']);
}

function gaBuyLink(){
    _gaq.push(['_trackEvent', 'button', 'buy']);
}

function onYouTubePlayerReady(playerId) {
    ytplayer = document.getElementById(playerId);
    ytplayer.addEventListener("onStateChange", "onytplayerStateChange");
}

function onytplayerStateChange(newState) {
    /* This event is fired whenever the player's state changes.
     Possible values are:
     - unstarted (-1)
     - ended (0)
     - playing (1)
     - paused (2)
     - buffering (3)
     - video cued (5).
     When the SWF is first loaded it will broadcast an unstarted (-1) event.
     When the video is cued and ready to play it will broadcast a video cued event (5).
     */
    if (newState == 1 || newState == 2) {
        gaVideoClick();
    }
}

function arrowCheck(delay){
    setTimeout(function(){
        if($('#current_item').html() == 1){
            $('.prev').hide();
        }else{
            $('.prev').show();
        }
        if($('#count_items').html() == $('#current_item').html()){
            $('.next').hide();
        }else{
            $('.next').show();
        }
    }, delay);

}





