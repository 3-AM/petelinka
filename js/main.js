$(document).ready(function(){

  $('#cabinet_form .fancy').fancySelect();

  $('.scroll-pane').jScrollPane();

  $('#winners_info > div').hide().eq(0).show();
  $('#winners_info >  div:not(:first)').hide();

  $('#winners_nav li').click(function(event) {
    event.preventDefault();
    $('#winners_info > div').hide();
    $('#winners_nav .current').removeClass("current");
    $(this).addClass('current');
    var clicked = $(this).find('a:first').attr('href');
    $(clicked +' tr:odd').addClass('bg_w');
    $('#winners_info ' + clicked).fadeIn('fast');
  });
  //$('#winners_info tr:nth-child(2)').addClass('main_winner');
    //$('#winners_info #week1 tr:odd').addClass('bg_w');
    //$('#winners_info div:first tr:odd').addClass('bg_w');

  /*check checked*/
  $(function(){
    $('input[type=checkbox]').change(function(){
      $(this).parent().css('','',this.checked);
    }).change();
  });
  $('input[type=checkbox]').change(function(){
    var self = $(this), chk = $(this).parent();
    chk.css('background-position', '0 ' + (self.prop("checked")? '-105' : '-65') +'px') ;
  });

  /*------ check radio on load*/
  $(function(){
    $('input[name="user_register[sex]"]').each(function(){
      if($(this).is(':checked')) {
        $(this).parent('span').css('background-position', '0 -30px');
      }
    });
  });

  /*check radio*/
  $(function(){
    $('input[name="user_register[sex]"]').change(function(e){
      var self = $(this), chk = $(this).parent();
      $('input[name="user_register[sex]"]').parent('span').css('background-position', '0 0') ;
      chk.css('background-position', '0 ' + (self.prop("checked")? '-30' : '0') +'px') ;
    })
  });

  $('#change_password_btn').on('click', function(){
    $('.overlay_wrapper').css('display','block');
    $('#change_password').css('display','inline-block');
    return false;
  });
  /*$('#reg_code_btn').on('click', function(){
    $('.overlay_wrapper').css('display','block');
    $('#registration_code').css('display','inline-block');
    return false;
  });*/
  $('.feedback_btn').on('click', function(){
    $('.overlay_wrapper').css('display','block');
    $('#feedback').css('display','inline-block');
    return false;
  });
  $('#log_btn').on('click', function(){
    $('.overlay_wrapper').css('display','block');
    $('#uthorization').css('display','inline-block');

    return false;
  });
  $('#restore_password_btn').on('click', function(){
    $('.overlay_wrapper').css('display','block');
    $('#change_password').css('display','inline-block');
    return false;
  });

  $('.close').on('click', function(){
    $(this).parent().css('display','none');
    $('.overlay_wrapper').css('display','none');
  });

  $('.trigger').on('click', function(){
    $(this).removeClass('error');

  });

  $('.required').on('focus', function(){
    if ($(this).hasClass('error')){
      $(this).removeClass('error').val('');
    }

  });

  $('.choose_city .options li').on('click', function(){
    var self = $(this),
      nmb = self.attr('data-value');
    if (nmb == 1){
      $('.other_city').css('display', 'block');
    }
    else {
      $('.other_city').css('display', 'none');
    }
    if (nmb == 2){
      $('.metro_station').css('display', 'block');
    }
    else {
      $('.metro_station').css('display', 'none');
    }

  });


  $('#registration_form').on('submit', function(){
    var subForm = $(this),
      errorMess = $('.error_message'),
      userName = $('input[name=user_name]'),
      userSecName = $('input[name=user_sname]'),
      userPhone = $('#user_register_username'),
      userEmail = $('#user_register_email'),
      userPassword = $('input[name=password]'),
      userPasswordRep = $('input[name=repeat_password]'),
      userRegion = $('.region-selector'),
      userCity = $('.settlement-selector'),
      userOtherCity = $('input[name=other_city]'),
      userStreet = $('input[name=street]'),
      userHouse = $('input[name=house]'),
      is_valid = true;

    errorMess.html('');

    /*if(!userName.val().length || userName.val() == 'Введите данные'){
        userName.addClass('error').val('Введите данные');
      is_valid = false;
    }
    if(! /^[а-яА-ЯёЁ]+$/.test(userName.val())){
      errorMess.append('Имя должно содержать только кириллические символы<br>');
      userName.addClass('error');
      is_valid = false;
    }

    if(!userSecName.val().length || userSecName.val() == 'Введите данные'){
      userSecName.addClass('error').val('Введите данные');
      is_valid = false;
    }
    if(! /^[а-яА-ЯёЁ]+$/.test(userSecName.val())){
      errorMess.append("Фамилия должна содержать только кириллические символы<br>");
      userSecName.addClass('error');
      is_valid = false;
    }

    if(!userPhone.val().length || !userPhone.val() == 'Введите данные'){
      userPhone.addClass('error').val('Введите данные');
      is_valid = false;
    }
    if(!/^\d{3}-\d{3}-\d{2}-\d{2}$/.test(userPhone.val())){
      errorMess.append("Номер телефона задан в неверном формате<br>");
      userPhone.addClass('error');
      is_valid = false;
    }

    if(!userEmail.val().length){
      userEmail.addClass('error').val('Введите email');
      is_valid = false;
    }
    if(!/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(userEmail.val())){
      errorMess.append("Email задан в неверном формате<br>");
      userEmail.addClass('error');
      is_valid = false;
    }

    if(!userPassword.val().length){
      userPassword.addClass('error');
      is_valid = false;
    }
    if(userPassword.val().length < 8){
      errorMess.append('Пароль должен состоять не менее чем из 8 символов<br>');
      userPassword.addClass('error');
      is_valid = false;
    }
    if(userPassword.val() != userPasswordRep.val()){
      errorMess.append('Пароли должны совпадать<br>');
      userPassword.addClass('error');
      userPasswordRep.addClass('error');
      is_valid = false;
    }


    if(!userRegion.val().length){
      errorMess.append('Вы должны выбрать регион<br>');
      userRegion.siblings('.trigger').addClass('error');
      is_valid = false;
    }

    if(!userCity.val().length){
      errorMess.append('Вы должны выбрать город<br>');
      userCity.siblings('.trigger').addClass('error');
      is_valid = false;
    }

    if(userOtherCity.parent().css('display') == 'block' && !userOtherCity.val().length){
      userOtherCity.addClass('error').val('Введите данные');
      is_valid = false;
    }

    if(!userStreet.val().length){
      userStreet.addClass('error').val('Введите данные');
      is_valid = false;
    }

    if(!userHouse.val().length){
      userHouse.addClass('error').val('Введите данные');
      is_valid = false;
    }

    if(!$('.check').prop("checked")){
      errorMess.append('Вы должны согласиться с условиями Акции<br>');
      is_valid = false;
    }
*/
    return is_valid? true: false;

  });

  $('#change_password_form').on('submit', function(){
    var errorMess = $('#change_password_form .error_message'),
      newPass = $('input[name=new_password]'),
      newPassRepeat = $('input[name=new_password_repeat]');
    errorMess.html('');

    if(!newPass.val().length, !newPassRepeat.val().length){
      errorMess.html('Введите новый пароль');
      return false
    }
    if(newPass.val().length < 8){
      errorMess.html('Пароль должен состоять не менее чем из 8 символов');
      return false
    }
    if(newPass.val() != newPassRepeat.val()){
      errorMess.html('Пароли должны совпадать');
      return false
    }
    return true;
  });

  $('#feedback_form').on('submit', function(){
    var is_feed_valid = true,
      errorMess = $('#feedback_form .error_message'),
    userFeedEmail = $('input[name=feed_email]'),
      userFeedName = $('input[name=feed_name]'),
      userFeedTheme = $('select[name=feed_theme]'),
      userFeedMessage = $('textarea[name=feed_message]');
    errorMess.html('');

    if(!userFeedEmail.val().length){
      userFeedEmail.addClass('error').val('Введите email');
      is_feed_valid = false;
    }
    if(!/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(userFeedEmail.val())){
      errorMess.append("Email задан в неверном формате<br>");
      userFeedEmail.addClass('error');
      is_feed_valid = false;
    }
    if(!userFeedName.val().length || userFeedName.val() == 'Введите имя') {
      userFeedName.addClass('error').val('Введите имя');
      errorMess.append("Введите имя пользователя<br>");
      is_feed_valid = false;
    }

    if(!userFeedTheme.val().length){
      errorMess.append('Выберите тему сообщения<br>');
      userFeedTheme.siblings('.trigger').addClass('error');
      is_valid = false;
    }

    if(!userFeedMessage.val().length || userFeedMessage.val() == 'Введите текст сообщения'){
      errorMess.append('Введите текст сообщения<br>');
      userFeedMessage.addClass('error').val('Введите текст сообщения');
      is_valid = false;
    }
    if(userFeedMessage.val().length < 10 ){
      errorMess.append('Тест сообщения должен быть не меннее 10 символов <br>');
      userFeedMessage.addClass('error');
      is_valid = false;
    }

    return is_feed_valid? true: false;
  });

  $('.jcarousel_promo').jcarousel({
    //wrap: 'last',
    wrap: 'circular',
    animation: {
      duration: 700,
      easing:   'linear'
    }

  }).jcarouselAutoscroll({
      interval: 5800,
      target: '+=1',
      autostart: true
    });

$('.jcarousel_promo').on('jcarousel:targetin', 'li', function(event, carousel) {
  if ($(this).find("video").size()) 
    $(this).find("video").get(0).play();
});

  /*------------ big-small images*/
  var currentBigImage = 0;

  $('.navigation_photo_winner ul li:first-child').addClass('active');

  $('.navigation_photo_winner li').on('click', function(){
    var $this = $(this),
        thisPos = $this.index();
        //thisSrc = $this.find('img').attr('src').replace('_sm','_b');
        //$bigImage = $this.parents('div[class^="winner_image"]').find('.big_image');


    $this.addClass('active');
    $this.siblings().removeClass('active');
    //$bigImage.attr('src',thisSrc);
    $('.big_image').hide();
    $('#photo-' + $(this).attr('data-id')).show();
    //currentBigImage = thisPos;
  });

  /*$('.big_image').on('click', function(){
    var smImageContainer = $(this).siblings('div[class^="navigation_photo_winner_wrapper"]'),
        numbOfSmallImg = smImageContainer.find('li').length,
        bigImgSrc;

    smImageContainer.find('li').removeClass('active');
    ++currentBigImage;
    if(currentBigImage == numbOfSmallImg){
      smImageContainer.find('li').eq(0).addClass('active');
      currentBigImage = 0;
    } else {
      smImageContainer.find('li').eq(currentBigImage).addClass('active');
    }
    bigImgSrc = smImageContainer.find('.active img').attr('src').replace('_sm','_b');
    $(this).attr('src',bigImgSrc);
  });*/

  $('.prev-stage, .next-stage, #winners_nav_stage li').on('click',function(){
    var src,
        currentStage = $('#winners_nav_stage').find('.active a').attr('data-id'),
        currentSlide = $('#mycarousel > ul > li ').eq(currentStage-1).addClass('dd');

    src = currentSlide.find('.navigation_photo_winner li').eq(0).find('img').attr('src');
    //src = src.replace('_sm','_b');
    currentSlide.find('.big_image').first().show();

    currentBigImage = 0;
    $('.navigation_photo_winner ul li').removeClass('active');
    $('.navigation_photo_winner ul li:first-child').addClass('active');
  });
  var radio = $(".radio_wrapper").find('.radio_wr');
    radio.click(function() {
        radio.removeClass("active").not(radio).add(this).addClass("active");
      });
    var radio = $(".question_wrap").find('label');
    radio.click(function() {
        radio.removeClass("active").not(radio).add(this).addClass("active");
      });
});
